<?php

final class ControleurClasse
{
    // Affichage du formulaire pour le fichier .csv et le nombre d'élèves désirés par groupes
    public function defautAction(Array $A_parametres = null, Array $A_postParams = null){
        Vue::montrer('classe/form', array('formData' =>  $A_postParams));
    }

    // Enregistrement du fichier .csv et retour de la vue des groupes
    public function traiterAction(){
        $groupes = $_POST['groupes'];
        $file = file_get_contents($_FILES['classeExcel']['tmp_name']);
        file_put_contents('./classe.csv', $file);
        $O_classe = new Classe();
        $liste = $O_classe->recupererFichierListe();
        Vue::montrer('classe/voir', array('listeGroupes' => $O_classe->generationGroupes($liste, $groupes), 
                                            'listeClasse' => $O_classe->formatListeClasse($liste)));
    }

}
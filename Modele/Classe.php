<?php

final class Classe
{
    // methode pour récuper le fichier .csv et le retourner dans un tableau
    public function recupererFichierListe()
    {
        $handle = fopen("./classe.csv", "r");
        $liste = [];
        while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
            $liste[] = $data;
        }
        return $liste;
    }

    // tri du tableau récupéré du fichier .csv et le tranforme en un tableau d'étudiants avec seulement {M/Mme}/NOM/Prenom
    public function listeClasse($listeParam){
        $listeATrier = $listeParam;
        $liste = [];
        foreach($listeParam as $element){
            array_push($liste, $element);
        }
        return $liste;
    }

    // formatage du tableau d'étudiants récupéré du fichier .csv de la façon suivante =>
    // liste = tableau[n][étudiant]
    // liste[n][0] = {M/Mme}
    // liste[n][1] = NOM
    // liste[n][2] = Prénom
    public function formatListeClasse($listeParam){
        $listeAFormater = $this->listeClasse($listeParam);
        $liste = [];
        foreach($listeAFormater as $i => $element){
            $element = explode(';', $listeAFormater[$i][0]);
            array_push($liste, $element);;
        }
        return $liste;
    }

    // méthode pour génerer les groupes aléatoirement en fonction du nombre maximum d'élèves désirés
    public function generationGroupes($liste, $n){
        $listeEtudiants = $this->listeClasse($liste);
        shuffle($listeEtudiants);
        $listeEtudiants = $this->formatListeClasse($listeEtudiants);
        $listeGroupes = array_chunk($listeEtudiants, $n);
        $listeGroupes = $this->triDernierGroupes($listeGroupes, $n);
        return $listeGroupes;
    }

    // tri du dernier groupe si il est inferieur aux autres
    // ajout des derniers élèves des premiers groupe au dernie groupe
    public function triDernierGroupes($liste, $n){
        $newListe = $liste;
        if (sizeof($liste[0]) != sizeof($liste[sizeof($liste) - 1])){
            $dernierGroupe = array_pop($newListe);
            foreach($newListe as $i => $groupe){
                $dernierEleveGroupe = array_pop($newListe[$i]);
                array_push($dernierGroupe, $dernierEleveGroupe);
                if(sizeof($dernierGroupe) == $n){
                    array_push($newListe, $dernierGroupe);
                    break;
                }
            }
        }
        return $newListe;
    }

}